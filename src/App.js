import React, { Fragment, useCallback, useState } from "react";
import {
  SwipeableList,
  SwipeableListItem,
} from "@sandstreamdev/react-swipeable-list";
import "@sandstreamdev/react-swipeable-list/dist/styles.css";

export let DESC = `Justo enim platea eget. Fusce tellus faucibus turpis nisi sit vel. Nunc rhoncus molestie curabitur. Aliquam quam quis urna proin. Rhoncus rutrum et non mattis congue ac. Consectetur condimentum suspendisse ex. Penatibus potenti elementum pulvinar massa.
Ipsum convallis ornare congue curabitur. Orci potenti consequat ultricies convallis tristique. Posuere tortor erat iaculis aliquet interdum egestas hendrerit. Ac at vestibulum penatibus a. Nascetur sed lacinia eros. Elit condimentum tristique quis dui diam etiam ac.`;
export let IMG =
  "https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/86/71/867186_v2.jpeg";

const tabs2 = [
  { title: "First Tab", description: DESC, sub: "1", background: "#eebcbc" },
  { title: "Second Tab", description: DESC, sub: "2", background: "#add" },
  { title: "Third Tab", description: DESC, sub: "3", background: "#adb" },
  { title: "For Tab", description: DESC, sub: "4", background: "#ff4d4f" },
  { title: "Five Tab", description: DESC, sub: "5", background: "#ad3" },
  { title: "Sex Tab", description: DESC, sub: "6", background: "#096dd9" },
  { title: "Seven Tab", description: DESC, sub: "7", background: "#bfbfbf" },
  { title: "Eight Tab", description: DESC, sub: "8", background: "#07e20d" },
];

function Layout() {
  const [active, setActive] = useState(0);
  const activeTab = useCallback((index) => {
    setActive(index);
  }, []);

  const nextPage = useCallback(() => {
    if (tabs2.length - 1 > active) {
      setActive(active + 1);
    }
  }, [active]);
  const prevPage = useCallback(() => {
    if (active > 0) {
      setActive(active - 1);
    }
  }, [active]);

  return (
    <div
      style={{
        padding: "0px",
        maxWidth: "420px",
        margin: "auto",
        background: "#ebf6fd",
        // background: "red",
      }}
    >
      <div
        style={{
          height: "80px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          margin: "0px 15px 15px 15px",
          // background: "red",
        }}
      >
        {tabs2.map((item, index) => {
          return (
            <div
              key={index}
              className={`${index === tabs2.length - 1 && "last"} ${
                index === active && "heightActive"
              }`}
              onClick={() => activeTab(index)}
              style={{
                borderRadius: `${index === 0 && "7px 0px 0px 7px"}`,
                height: "10px",
                width: `${100 / tabs2.length}%`,
                background: `${index === active ? "#47dbf8" : "#07a9b5"}`,
                margin: "1px",
                marginTop: "20px",
                marginBottom: "20px",
              }}
            />
          );
        })}
      </div>
      {/* ======================Tab======================*/}
      <div style={{ display: "flex", margin: "5px" }}>
        <SwipeableList>
          <SwipeableListItem
            threshold={0.1}
            swipeLeft={{
              content: "",
              action: () => nextPage(),
            }}
            swipeRight={{
              content: "",
              action: () => prevPage(),
            }}
          >
            {tabs2.map((item, index) => {
              return (
                <Fragment key={index}>
                  {index === active && (
                    <div
                      style={{
                        height: `${index % 2 === 0 ? "750px" : "1250px"}`,
                        // lineHeight: "250px",
                        textAlign: "center",
                        width: "100%",
                        background: item.background,
                      }}
                    >
                      <h1>
                        {item.title} {index}
                      </h1>
                    </div>
                  )}
                </Fragment>
              );
            })}
          </SwipeableListItem>
        </SwipeableList>
      </div>
      {/* ======================Tab======================*/}
    </div>
  );
}

export default Layout;
